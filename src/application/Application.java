/**
 * Created by cinemast on 10/20/14.
 */

package application;

import control.ExchangeCommand;
import model.CurrencySet;
import model.CurrencySetLoader;

public class Application {

    public static void main(String[] args) {

        CurrencySet currencySet = new CurrencySetLoader().load();
        ExchangeCommand command = new ExchangeCommand(currencySet);

        while (true) {
            command.execute();
        }

    }
}
