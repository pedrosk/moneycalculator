package model;

/**
 * Created by cinemast on 10/20/14.
 */
public class CurrencySetLoader {

    public CurrencySet load() {
        CurrencySet s = new CurrencySet();

        s.add(new Currency("USD", "US-Dollar", "$"));
        s.add(new Currency("EUR", "Euro", "€"));
        s.add(new Currency("GBP", "British Pounds", "£"));

        return s;
    }
}
