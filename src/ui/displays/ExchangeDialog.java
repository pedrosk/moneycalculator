package ui.displays;

import model.CurrencySet;
import model.Exchange;

/**
 * Created by cinemast on 10/20/14.
 */
public class ExchangeDialog {

    private final CurrencySet currencySet;
    private Exchange exchange;

    public ExchangeDialog(CurrencySet currencySet) {
        this.currencySet = currencySet;
    }

    public void execute() {

    }

    public Exchange getExchange() {

    }
}
