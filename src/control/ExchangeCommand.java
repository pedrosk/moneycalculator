package control;

import model.CurrencySet;
import model.Exchange;
import model.ExchangeRate;
import model.Money;
import ui.displays.ExchangeDialog;

public class ExchangeCommand {

    private final CurrencySet currencySet;

    public ExchangeCommand(CurrencySet currencySet) {
        this.currencySet = currencySet;
    }

    public void execute() {
        Exchange exchange = readExchange();
        ExchangeRate rate = readExchangeRate();
        Money money = exchange(exchange.getMoney(), rate);
        displayMoney(money);
    }

    private Money exchange(Money money, ExchangeRate rate) {
    }

    private void displayMoney(Money money) {
    }

    private ExchangeRate readExchangeRate() {
        return null;
    }

    private Exchange readExchange() {

        ExchangeDialog dialog = new ExchangeDialog(currencySet);
        dialog.execute();
        return dialog.getExchange();
    }
}
